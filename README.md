# Example RestAPI

Example RestAPI is an API which allows a user to query a database (or csv) and get results in either JSON or as points on a map.

## Usage

This is meant to be run locally, from the `app.py` file in the top-level directory.
Simply clone the repo to your local machine, navigate to the directory, and type:
```
pip install -r requirements.txt
```
followed by:
```
python app.py
```

The `flask_config.cfg` file in the `project` folder contains configuration options so you can supply the app with the proper paths to a csv file or PostGres database (with the PostGIS extension).

## Help, XX package won't install!

Some GIS-related package like GDAL can fail installation on Windows due to various reasons. If you are having trouble with a particular package not installing, try building from the appropriate wheel on Christoph Gohlke's well-maintained [list of wheels](https://www.lfd.uci.edu/~gohlke/pythonlibs/).

## Alternative Dockerfile
This is coming soon! Stay tuned for more!   

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)


