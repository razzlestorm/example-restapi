from project import db
from geoalchemy2 import Geometry


class Event(db.Model):
    __tablename__ = "events"
    id = db.Column(db.Integer, primary_key=True)
    # TODO: convert to epoch
    datetime = db.Column(db.String(32), nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    description = db.Column(db.String(255), nullable=False)
    # TODO: create a lookup table
    county_name = db.Column(db.String(255), nullable=False)
    shape = db.Column(Geometry("MULTIPOLYGON", 4326))


    def __init__(self, id, datetime, longitude, latitude, description, county_name, shape):
        self.id = id
        self.datetime = datetime
        self.longitude = longitude
        self.latitude = latitude
        self.description = description
        self.county_name = county_name
        self.shape = shape

    def __repr__(self):
        return f'<Event ID {self.id}: {self.description}>'
