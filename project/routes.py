from flask import Flask, flash, render_template, redirect, request, json, Response, url_for
from project import app, gdf, MAPBOX_TOKEN
from project.utils import convert_data, date_parser

# Routes:
@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        start_date, end_date = request.form.get("query-start"), request.form.get("query-end")
        counties = request.form.getlist("counties")
        display = request.form.get("display")
        return redirect(url_for('query', start_date=start_date, end_date=end_date, counties=counties, display=display))

    return render_template('index.html', title='Home', counties=gdf['county_name'].value_counts().keys())

@app.route('/query', methods=['GET'])
def query():
    '''
    route handling database/dataframe queries
    optional params:
    # TODO: Validate/test parameters
    - start_date: This will ideally be passed as a date in the format {YYYY-MM-DD} (test for this, potentially editing entry if input backwards).
    - end_date: This will ideally be passed as a date in the format {YYYY-MM-DD} (test for this, potentially editing entry if input backwards).
    NOTE: Both start_date and end_date are required to filter by date (otherwise will only filter by counties)
    - counties: Should just be a list of name of counties to filter on.
    - display: Defaults to "json", looks for the keyword "map". If set to "map", will render map with data (any other value but "map").
    WARNING: If no parameters are passed, will return ALL DATA
    Example query:
    html://sitename.com/query?start_date=2020-01-01&end_date=2020-12-31&counties=PALM+BEACH&counties=OKALOOSA&display=json
    '''
    dates = (request.args.get('start_date'), request.args.get('end_date'))
    counties = request.args.getlist('counties')
    display = request.args.get('display')
    if all(dates) and counties:
        dates = date_parser(dates)
        data = gdf[(dates.start <= gdf['datetime']) & (gdf['datetime'] <= dates.end)].loc[gdf['county_name'].isin([county for county in counties])]
    if counties and not all(dates):
        data = gdf.loc[gdf['county_name'].isin([county for county in counties])]
    if all(dates) and not counties:
        dates = date_parser(dates)
        data = gdf[(dates.start <= gdf['datetime']) & (gdf['datetime'] <= dates.end)]
    
    if len(data) == 0:
        flash("Your search returned 0 results, please try adjusting your dates or counties")
        return redirect(url_for('home'))

    data['shape'] = convert_data(data['shape'])
    parser = json.loads(data.to_json())
    # custom geojson class, based on GeoJSON as used by mapbox
    geojson = {
        "type": "FeatureCollection",
        "features": [
                    {
                        "type": "Feature",
                        "geometry" : {
                            "type": entry["properties"]["shape"]["type"],
                            "coordinates":  entry["properties"]["shape"]["coordinates"]
                                    },
                        "properties" : {k:v for k, v in entry["properties"].items() if k != "shape"}
                        
                } for entry in parser["features"]]
                    }
    if display == "map":
        return render_template('query.html', data=geojson, token=MAPBOX_TOKEN)
    return Response(json.dumps(geojson), mimetype='application/json')
    # The following works, but passes GeoJSON in a non-standard format (with properties first): 
    # return Response(data.to_json(), mimetype='application/json')
