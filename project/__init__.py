from decouple import config
from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy

from project.utils import *


app = Flask(__name__, template_folder="templates")
app.config.from_pyfile('flask_config.cfg')
app.debug = app.config['DEBUG_MODE']
app.config['SECRET_KEY'] = config("SECRET_KEY")
database_uri = app.config['SQLALCHEMY_DATABASE_URI'] = config("SQLALCHEMY_DATABASE_URI")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
MAPBOX_TOKEN = config("MAPBOX_TOKEN")

db = SQLAlchemy(app)

# Debug toolbar will only appear when Debug_Mode = True
toolbar = DebugToolbarExtension(app)
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

if app.config['DATA_TYPE'] == 'csv':
    gdf = load_data(app.config['DATA_PATH'])
if app.config['DATA_TYPE'] == 'postgres':
    # The plan if this is true is to:
    ## query database directly and return results via a different route
    pass

from project import routes