from typing import NamedTuple, Tuple
from flask import json
import geopandas as gpd


# Utility functions:
class DateRange(NamedTuple):
    start: str
    end: str

def convert_data(gs: gpd.GeoSeries) -> gpd.GeoSeries:
    '''
    This takes in a geoseries with entries that are in string format,
    and converts them to objects.
    '''
    gs = gs.apply(lambda x: json.loads(x))
    return gs

def load_data(rel_path: str) -> gpd.GeoDataFrame:
    '''
    This function can be changed to use Pandas or GeoPandas,
    and makes it so only one location needs to be changed.
    Potentially a bit redundant.
    '''
    return gpd.read_file(rel_path)

# Now redundant, consider removing
def date_parser(date: Tuple) -> DateRange:
    '''
    This parses a string argument and returns a DateRange from it.
    Expects a stringified tuple like: (YYYY-MM-DD, YYYY-MM-DD)
    '''
    return DateRange(date[0], date[1])