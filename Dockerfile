# TODO: switch to ubuntu and get working
FROM python:alpine

MAINTAINER Jerimiah Willhite "willhite.jerimiah@gmail.com"

COPY ./requirements.txt /requirements.txt

RUN apk add --no-cache gdal

# Update C env vars so compiler can find gdal
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal

WORKDIR /

RUN pip3 install -r requirements.txt



COPY . /

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]