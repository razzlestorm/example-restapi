from project import app

if __name__ == "__main__":
    app.run(debug=app.debug, port=5000)